package controllers;

import java.io.Serializable;
import java.text.SimpleDateFormat;
import java.util.Calendar;

import javax.faces.bean.ManagedBean;
import javax.faces.bean.RequestScoped;

@ManagedBean(name = "controlePrincipal")
@RequestScoped
public class ControlePrincipal implements Serializable {

	private static final long serialVersionUID = 1L;
	private String mensagem;
	
	public ControlePrincipal() {
		SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyy HH:m:ss:S");
		mensagem = "A aplica��o foi ao ar " +
				sdf.format(Calendar.getInstance().getTime());
	}
	
	public String home() {
		return "/index";
	}
	
	public String sobre() {
		mensagem = "Voc� navegou de maneira dinamica";
		return "sobre";
	}
	
	public String sobreRedirecionado() {
		mensagem = "Voc� foi redirecionado para a pagina sobre.xhtml";
		return "sobre?faces-redirect=true";
	}
	
	public String geraErroGrave() {
		return "erro";
	}
	
	public String geraErro() {
		return "erro";
	}
	
	public String geraLogin() {
		return "login";
	}

	public String getMensagem() {
		return mensagem;
	}

	public void setMensagem(String mensagem) {
		this.mensagem = mensagem;
	}
	
}