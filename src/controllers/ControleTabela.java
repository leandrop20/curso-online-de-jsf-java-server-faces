package controllers;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import javax.faces.bean.ManagedBean;
import javax.faces.bean.SessionScoped;

import entities.Pessoa;

@ManagedBean(name="controleTabela")
@SessionScoped
public class ControleTabela implements Serializable {
	private static final long serialVersionUID = 1L;
	
	private List<Pessoa> lista;
	
	public ControleTabela() {
		lista = new ArrayList<Pessoa>();
		lista.add(new Pessoa(1, "Jo�o", "(00)0000-0000"));
		lista.add(new Pessoa(2, "Antonio", "(00)0000-0000"));
		lista.add(new Pessoa(3, "Maria", "(00)0000-0000"));
		lista.add(new Pessoa(4, "Jos�", "(00)0000-0000"));
		lista.add(new Pessoa(5, "Cleide", "(00)0000-0000"));
		lista.add(new Pessoa(6, "Clemilda", "(00)0000-0000"));
		lista.add(new Pessoa(7, "Serafina", "(00)0000-0000"));
		lista.add(new Pessoa(8, "Carla", "(00)0000-0000"));
	}
	
	public String salvar() {
		for (Pessoa obj : lista) {
			obj.setEditando(false);
		}
		return null;
	}
	
	public String excluir(Pessoa obj) {
		lista.remove(obj);
		return null;
	}

	public List<Pessoa> getLista() {
		return lista;
	}

	public void setLista(List<Pessoa> lista) {
		this.lista = lista;
	}
	
}
