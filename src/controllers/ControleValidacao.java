package controllers;

import java.io.Serializable;

import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.RequestScoped;
import javax.faces.context.FacesContext;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Pattern;

import org.hibernate.validator.constraints.Email;
import org.hibernate.validator.constraints.Length;
import org.hibernate.validator.constraints.NotEmpty;
import org.hibernate.validator.constraints.Range;

@ManagedBean(name="controleValidacao")
@RequestScoped
public class ControleValidacao implements Serializable {

	private static final long serialVersionUID = 1L;
	@Length(max=40, min=3, message="M�nimo {min}, m�ximo {max}!")
	@NotEmpty(message="O campo Nome deve ser informado!")
	private String nome;
	@NotEmpty(message="O campo Telefone deve ser informado!")
	@Pattern(regexp="\\(\\d{2}\\)\\d{4}\\-\\d{4}", message="Formato Inv�lido!")
	private String telefone;
	@Email(message="Email Invalido!")
	@NotEmpty(message="O campo E-mail deve ser informado!")
	private String email;
	@NotNull(message="O campo Idade deve ser informado!")
	@Range(min=12, max=80, message="M�nimo {min}, m�ximo {max}")
	private Long idade;
	@Range(min=0, max=19, message="M�nimo {min}, m�ximo {max}")
	private Double nota;
	
	public ControleValidacao() {
		
	}
	
	public String processaDados() {
		String saida = "";
		saida += "Nome: " + nome;
		saida += "Telefone: " + telefone;
		saida += "Email: " + email;
		saida += "Idade: " + idade;
		saida += "Nota: " + nota;
		FacesMessage msg = new FacesMessage(saida);
		FacesContext.getCurrentInstance().addMessage("", msg);
		nome = "";
		telefone = "";
		email = "";
		idade = null;
		nota = null;
		return "formValidacao";
	}
	
	public String getNome() {
		return nome;
	}
	
	public void setNome(String nome) {
		this.nome = nome;
	}
	
	public String getTelefone() {
		return telefone;
	}
	
	public void setTelefone(String telefone) {
		this.telefone = telefone;
	}
	
	public String getEmail() {
		return email;
	}
	
	public void setEmail(String email) {
		this.email = email;
	}
	
	public Long getIdade() {
		return idade;
	}
	
	public void setIdade(Long idade) {
		this.idade = idade;
	}
	
	public Double getNota() {
		return nota;
	}
	
	public void setNota(Double nota) {
		this.nota = nota;
	}
	
}
